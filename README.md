# 開発メモ

## スキル設定

### インテントスキーマ

```JSON
{
  "intents": [
    {
      "intent": "GetBalance"
    },
    {
      "intent": "AMAZON.HelpIntent"
    },
    {
      "intent": "AMAZON.StopIntent"
    },
    {
      "intent": "AMAZON.CancelIntent"
    },
    {
      "intent": "AMAZON.YesIntent"
    },
    {
      "intent": "AMAZON.NoIntent"
    },
    {
      "intent": "GetCoin",
      "slots": [
        {
          "name": "Coin",
          "type": "LIST_OF_COINS"
        }
      ]
    },
    {
      "intent": "UpdateWallet",
      "slots": [
        {
          "name": "Coin",
          "type": "LIST_OF_COINS"
        }
      ]
    },
    {
      "intent": "SpeechNumber",
      "slots": [
        {
          "name": "Num",
          "type": "AMAZON.NUMBER"
        }
      ]
    }
  ]
}
```

### LIST_OF_COINS

```
ビットコイン
イーサリアム
イーサリアムクラシック
リスク
ファクトム
モネロ
オーガー
ネム
リップル
ゼットキャッシュ
ライトコイン
ダッシュ
ビットコインキャッシュ
ビット
イーサ
イークラ
イーサクラシック
ゼット
ライト
ビッチ
モナコイン
モナ
モナコ
```

### サンプル発話

```
GetBalance 総資産を教えて
GetBalance 残高を教えて
GetBalance 残高は
GetBalance 評価額
GetBalance 総資産
GetCoin {Coin} の値段は
GetCoin {Coin} の値段が知りたい
GetCoin {Coin} の相場は
GetCoin {Coin} のレートは
GetCoin {Coin} のレートを知りたい
GetCoin {Coin} はいくら
GetCoin {Coin} の評価額は
UpdateWallet {Coin} の残高を変更
UpdateWallet {Coin} の所持数を変更
UpdateWallet {Coin} の所持量を変更
UpdateWallet {Coin} を更新
SpeechNumber {Num}
AMAZON.StopIntent 終了して
AMAZON.StopIntent 終了
AMAZON.StopIntent おわり
```


## 申請用

### スキルの簡単な説明

```
仮想通貨のレートをチェックするスキルです。
各通貨の所持数を設定しておくと総資産もチェック可能です。
```

### スキルの詳細な説明

```
CoinCheck/Zaifの公開APIを利用した仮想通貨のレート確認アプリです。
各コイン所持数を設定しておく事で総資産評価を計算する事も可能です。

対応通貨はBitcoin, Ethereum, Ethereum Classic, Lisk, Factom, Monero, Augur, Ripple, Zcash, NEM, Litecoin, Dash, Bitcoin Cash, Mona です。

本スキルを利用したことに起因するいかなる損害についても一切責任を負いません。
個人的に作成しているスキルの為、予告なく機能の変更/追加が発生する事があります。

〇 所持コインの設定
 【アレクサ、仮想通貨ポートフォリオでビットコインの所持数を変更したい】
 【アレクサ、仮想通貨ポートフォリオでリップルの残高を変更】
 指定したコインの所持数を変更できます。
 コイン所持数の整数部・小数部の順に求められるので順番に読み上げ、更新を承認するとコイン所持数の変更が確定します。整数部・小数部共に0を設定するとコインの登録を削除します。

〇 通貨レートの取得
 【アレクサ、仮想通貨ポートフォリオでビットコインの値段を教えて】
 指定したコインの通貨レート・騰落率を取得します。コインの所持数を設定済みの場合、追加で評価額を計算します。

〇 総資産の取得
【アレクサ、仮想通貨ポートフォリオで総資産を教えて】
 設定済み所持コインと各コインのレートから現在の評価額と騰落率を計算します。
 続けて各通貨のレートをチェックすると所持しているコインそれぞれの通貨レート・騰落率を読み上げます。

※ 騰落率は24時間前のレートと現在のレートを比較して算出します。

```

## ステート遷移/発話内容メモ

(※) DEFAULT_PROMPT = '総資産を尋ねるか、レートを知りたいコインの名前をおっしゃって下さい。'
(※) GOODBYE_MESSAGE = 'さようなら。またのご利用お待ちしております。'

* default

|Intent|発話内容|次ステート/備考|
|:--|:--|:--|
|LaunchRequest|仮想通貨ポートフォリオスキルへようこそ。 + DEFAULT_PROMPT||
|StopIntent|GOODBYE_MESSAGE|セッション終了|
|HelpIntent|ヘルプ文言 + DEFAULT_PROMPT||
|GetBalance(所持コイン設定あり)|現在の総資産は前日よりxx%{上がって/下がって} xxx円です。各通貨のレートをチェックしますか？|GETDETAIL|
|GetBalance(所持コイン設定なし)|所持コインが設定されていません。例えば「ビットコインの所持数を変更」のように話しかけてコインの所持数の設定を行ってください。||
|GetCoin(Coinスロット正当)|通貨詳細読み上げ + DEFAULT_PROMPT|カード出力|
|GetCoin(Coinスロット不正)|コイン名の認識に失敗しました。もう一度レートを知りたいコインの名前をおっしゃって下さい。||
|UpdateWallet(Coinスロット正当)|"現在{Coin}をxxxコイン所持しています" or "現在{Coin}は所持していません"。コイン所持数の整数部分を読み上げてください|UPDATEWALLET_INT|
|UpdateWallet(Coinスロット不正)|コイン名の認識に失敗しました。もう一度所持コイン数の変更をおっしゃって下さい。||
|GetCoin/GetBalance WebAPI呼び出し失敗|xxx取得に失敗しました。もう一度レートを知りたいコインの名前をおっしゃって下さい。||
|Unhandled|すいません、よくわかりません。 + DEFAULT_PROMPT|

* GETDETAIL

|Intent|発話内容|次ステート/備考|
|:--|:--|:--|
|YesIntent|{所持通貨詳細読み上げ} 以上です。またのご利用お待ちしております。|default・セッション終了|
|NoIntent<br>CancelIntent<br>Unhandled|各通貨レートの確認を中止し、終了します。またのご利用お待ちしております。|default・セッション終了|
|StopIntent|GOODBYE_MESSAGE|default・セッション終了|

* UPDATEWALLET_INT

|Intent|発話内容|次ステート/備考|
|:--|:--|:--|
|SpeechNumber(Num不正)|数値が認識できませんでした。もう一度コイン所持数の整数部を読み上げてください。||
|SpeechNumber(Num正当)|{Num}ですね。続けて小数部を読み上げてください|UPDATEWALLET_DEC|
|StopIntent|GOODBYE_MESSAGE|default・セッション終了|
|CancelIntent|所持コインの更新を中止します。+ DEFAULT_PROMPT|default|
|Unhandled|コイン所持数の整数部を読み上げてください。||

* UPDATEWALLET_DEC

|Intent|発話内容|次ステート/備考|
|:--|:--|:--|
|SpeechNumber(Num不正)|数値が認識できませんでした。もう一度コイン所持数の小数部を読み上げてください。||
|SpeechNumber(Num正当)|{Coin}の所持数をxxxに更新します。よろしいですか？|UPDATEWALLET_CONFIRM|
|StopIntent|GOODBYE_MESSAGE|default・セッション終了|
|CancelIntent|所持コインの更新を中止します。+ DEFAULT_PROMPT|default|
|Unhandled|コイン所持数の小数部を読み上げてください。||

* UPDATEWALLET_CONFIRM

|Intent|発話内容|次ステート/備考|
|:--|:--|:--|
|YesIntent|{Coin}の所持数をxxxに更新しました。 + DEFAULT_PROMPT|default|
|NoIntent<br>CancelIntent|所持コインの更新を中止します。+ DEFAULT_PROMPT|default|
|StopIntent|GOODBYE_MESSAGE|default・セッション終了|
|Unhandled|はい、もしくはいいえで答えてください。||

### 通貨詳細読み上げ

「{coin}のレートは{騰落メッセージ}xx円 {評価額メッセージ}」

* 騰落メッセージ

前レートが存在する時に挿入。「前日からxx%{上がって/下がって}」

* 評価額メッセージ

{coin} を所持している時に挿入。「で、評価額はxx円です。」
{coin} 未所持なら単に「です。」

# バージョン履歴

### 1.0.0

* 初公開

### 1.0.1

* モナコイン対応
* 資産変動を前回取得時の差分から、履歴テーブルを使用した24時間前レートでの評価額との差分に変更
* 各通貨詳細も同じく24時間前からの騰落率を追加
* GetBalance, GetCoin時にカードを出力
* 会話シーケンス変更。