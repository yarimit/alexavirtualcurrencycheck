function Ticker(coinCheck) {
    this._coinCheck = coinCheck;
}

Ticker.prototype = {
    urlRoot: '/api/ticker',
    all: function(params) {
        return this._coinCheck.request('get', this.urlRoot, params);
    },
    rate: function(params, pair) {
        return this._coinCheck.request('get', "/api/rate/" + pair, params);
    }
};

exports.Ticker = Ticker;