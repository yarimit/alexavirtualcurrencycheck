const Alexa = require('alexa-sdk');
var APP_ID = undefined;

// コイン定義/レート取得API
const VC = require('./virtual-currency.js');
const coins = VC.COINS;

// 過去レート取得
const DIFF_HOURS = 24;
const PREV_DESC = "前日";
const RH = require('./rate-history.js');
var rateHistory = new RH.RateHistory(process.env.RATE_HISTORY_TABLE, DIFF_HOURS);

// ステートの定義
const states = {
    GETDETAIL: "_GETDETAIL",
    UPDATEWALLET_INT: "_UPDATEWALLET_INT",
    UPDATEWALLET_DEC: "_UPDATEWALLET_DEC",
    UPDATEWALLET_CONFIRM: "_UPDATEWALLET_CONFIRM"
};

const GOODBYE_MESSAGE = 'さようなら。またのご利用お待ちしております。';
const DEFAULT_PROMPT = '総資産を尋ねるか、レートを知りたいコインの名前をおっしゃって下さい。';

function lookupCoin(coinSlot) {
    if (coinSlot && coinSlot.value) {
        return coins.find((c) => {
            return (c.name === coinSlot.value ||
                c.alias.indexOf(coinSlot.value) > -1);
        });
    }
    return null;
}

function codeToName(code) {
    var coin = coins.find((c) => { return c.code === code; });
    if (coin) {
        return coin.name;
    }
    return "";
}

function logPrefix(_this, intent) {
    try {
        return `${intent}:UID(${_this.event.context.System.user.userId}) `;
    } catch (error) {
        console.error("logPrefix: Can't get UserID");
        return `${intent}:UID(unknown)`;
    }
}

function currencyFormat(num) {
    var delimiter = '.',
        numString = num.toString(),
        delimitExp = new RegExp('(\\d)(?=(\\d{3})+$)', 'g'), // 整数フォーマット。
        decimalDelimitExp = new RegExp('(\\d)(?=(\\d{3})+(\\.\\d+))', 'g'); // 小数フォーマット。

    function isDecimal() {
        if (numString.indexOf(delimiter) > -1) {
            return true;
        } else {
            return false;
        }
    }

    if (isDecimal()) {
        return numString.replace(decimalDelimitExp, '$1,');
    } else {
        return numString.replace(delimitExp, '$1,');
    }
}

// コインのレート用メッセージ作成
// {coin}のレートはxxよりxx% {上がって/下がって} xx円で、評価額は xx円です。
// {coin}のレートはxxよりxx% {上がって/下がって} xx円です。
// {coin}のレートはxx円です。
function coinRateMessage(coin, rate, prevRate, wallet) {
    // 現在価格
    var diffMessage = "";
    if(prevRate !== 0) {
        // 前のレートが取れている時だけ騰落率出力
        let d = (rate >= prevRate) ? '上がって' : '下がって';
        let diff = Math.abs(Math.floor( ((rate - prevRate) / prevRate) * 100 * 100) / 100);
        diffMessage = `${PREV_DESC}より${diff}％${d}`;
    }
    var message = `${coin.name}のレートは${diffMessage}${rate}円`;

    if (wallet[coin.code]) {
        // 評価額
        let yourcoin = wallet[coin.code];
        let hyouka = Math.floor(yourcoin * rate);
        message += `で、評価額は${hyouka}円です。`;
    } else {
        message += `です。`;
    }
    return message;
}

const handlers = {
    'LaunchRequest': function () {
        this.emit(':ask', '仮想通貨ポートフォリオスキルへようこそ。' + DEFAULT_PROMPT);
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell', GOODBYE_MESSAGE);
    },
    'AMAZON.HelpIntent': function () {
        this.emit(':ask', '仮想通貨の価格や評価額をチェックします。' +
            'たとえば、「ビットコインのレートを教えて」と聞いてください。' +
            'また、「ビットコインの所持数を変更」などと話しかけて所持コインを設定しておくと' +
            '「総資産を教えて」と聞かれた時に日本円での合計評価額をお伝えします。' +
            DEFAULT_PROMPT
        );
    },
    'GetBalance': function () {
        var _this = this;
        const userId = this.event.context.System.user.userId;

        // 所持コインの種類数
        var wallet = this.attributes["wallet"];
        if (!wallet) {
            wallet = {};
        }

        var coinTypeCnt = coins.filter((coin) => {
            return (wallet[coin.code] && wallet[coin.code] > 0);
        }).length;

        if (coinTypeCnt == 0) {
            // 所持コイン設定なし
            console.warn(`${logPrefix(_this, 'GetBalance')} 所持コイン未設定`);
            _this.emit(':ask', `所持コインが設定されていません。例えば「ビットコインの所持数を変更」のように話しかけてコインの所持数の設定を行ってください。`);
            return;
        }

        // 前のレート取得
        rateHistory.getRateAll().then( (prevRates) => {
            // 全過去レートが存在するかフラグ
            var existAllPrevRates = true;

            // 現在のレートを取得して各通貨情報を詰めていく
            new Promise(function (resolve, reject) {
                var balance = [];
                coins.forEach((coin) => {
                    if (wallet[coin.code] && wallet[coin.code] > 0) {
                        VC.getRate(coin).then((rate) => {
                            let frate = Math.floor(rate * 100) / 100;
                            let prevRate = prevRates[coin.code] ? Math.floor(prevRates[coin.code] * 100) / 100 : 0;
                            existAllPrevRates = prevRates[coin.code] ? existAllPrevRates : false;
                            var yourcoin = wallet[coin.code];

                            balance.push({
                                coin: coin,
                                hasCoin: yourcoin,
                                rate: frate,
                                prevRate : prevRate,
                                jpy: Math.floor(yourcoin * frate),
                                prevJpy: Math.floor(yourcoin * prevRate)
                            });
    
                            if (coinTypeCnt == Object(balance).length) {
                                resolve(balance);
                            }
                        }, (err) => {
                            reject(err);
                        });
                    }
                });
            }).then((balance) => {
                // order順にソートしておく
                balance.sort((a, b) => {
                    return a.coin.order > b.coin.order;
                });
    
                let totalJpy = Math.floor(
                    balance.reduce((a, x) => a += x.jpy, 0)
                );
                let totalJpyPrev = Math.floor(
                    balance.reduce((a, x) => a += x.prevJpy, 0)
                );

                // 騰落率
                var diffMessage = '';
                var diffCardMessage = '';
                if(existAllPrevRates) {
                    // 全過去レートが存在した時だけ出力
                    let diffSign = (totalJpy >= totalJpyPrev) ? '+' : '-';
                    let diff = Math.abs(Math.floor( ((totalJpy - totalJpyPrev) / totalJpyPrev) * 100 * 100) / 100);
                    diffCardMessage = `(${diffSign} ${diff}%)`;
    
                    let d = (totalJpy >= totalJpyPrev) ? '上がって' : '下がって';
                    diffMessage = `${PREV_DESC}より${diff}％${d}`;
                    console.info(`${logPrefix(_this, 'GetBalance')} success. totalJpy=${totalJpy}, diff=${diffSign}${diff}%`);
                } else {
                    console.info(`${logPrefix(_this, 'GetBalance')} success. totalJpy=${totalJpy}, no diff`);
                }

                let message = `現在の総資産は${diffMessage}${totalJpy}円です。`;
    
                // カードには各通貨詳細を出力
                let cardTitle = `総資産評価額`;
                var cardContent = `${currencyFormat(totalJpy)} 円 ${diffCardMessage}\n`;
                var cardDetails = balance.map((b) => {
                    return `${b.coin.code.toUpperCase()} : ${currencyFormat(b.jpy)} 円 (${currencyFormat(b.rate)} x ${b.hasCoin} coin)`;
                });
                cardContent += cardDetails.join('\n');
    
                console.info(`${logPrefix(_this, 'GetBalance')} success. state -> GETDETAIL`);
                let getDetailMessage = "各通貨のレートをチェックしますか？";
                this.handler.state = states.GETDETAIL;
                this.attributes["detail"] = balance;
                
                // 1.0.1以降削除するデータ
                this.attributes["prev_total_jpy"] = undefined;

                // 使用状況更新
                this.attributes["totalJpy"] = totalJpy;
                this.attributes["lastAccess"] = new Date();

                if(this.attributes["accessCount"]) {
                    this.attributes["accessCount"] += 1;
                } else {
                    this.attributes["accessCount"] = 1;
                }

                this.emit(':askWithCard',
                    message + getDetailMessage,
                    getDetailMessage,
                    cardTitle,
                    cardContent
                );
            }, (error) => {
                console.error(`${logPrefix(_this, 'GetBalance')} ${error}`);
                _this.emit(':ask', `総資産の取得に失敗しました。もう一度レートを知りたいコインの名前をおっしゃって下さい。`);
            });
        });
    },

    'GetCoin': function () {
        var _this = this;
        const coinSlot = this.event.request.intent.slots.Coin;
        let coin = lookupCoin(coinSlot);
        if (!coin) {
            _this.emit(':ask', 'コイン名の認識に失敗しました。もう一度レートを知りたいコインの名前をおっしゃって下さい。');

            if (coinSlot && coinSlot.value) {
                console.warn(`${logPrefix(_this, 'GetCoin')} Unknown coin [${coinSlot.value}]`);
            } else {
                console.warn(`${logPrefix(_this, 'GetCoin')} Unknown coin [No Coin slot]`);
            }
            return;
        }

        VC.getRate(coin).then((rate) => {
            rateHistory.getRate(coin.code).then((prev_rate) => {
                // 丸める
                let frate = Math.floor(rate * 100) / 100;
                let prevFrate = Math.floor(prev_rate * 100) / 100;

                // 騰落率
                var diffSign, diff;
                var diffCardMessage = ""; // カード出力用騰落率
                if(prev_rate !== 0) {
                    diffSign = (frate >= prevFrate) ? '+' : '-';
                    diff = Math.abs(Math.floor( ((frate - prevFrate) / prevFrate) * 100 * 100) / 100);
                    diffCardMessage = `(${diffSign} ${diff}%)`;
                }

                var wallet = this.attributes["wallet"];
                if (!wallet) {
                    wallet = {};
                }
    
                // レスポンス構築
                let cardTitle = `${coin.name}(${coin.code.toUpperCase()})`;
                var cardContent = `1${coin.code.toUpperCase()} = ${currencyFormat(frate)}JPY ${diffCardMessage}`;

                if (wallet[coin.code]) {
                    // 評価額
                    let yourcoin = wallet[coin.code];
                    let hyouka = Math.floor(yourcoin * frate);
                    cardContent += `\n評価額 ${currencyFormat(hyouka)} 円 (${yourcoin} coin)`;
                }

                // 使用状況更新
                this.attributes["lastAccess"] = new Date();
                if(this.attributes["accessCount"]) {
                    this.attributes["accessCount"] += 1;
                } else {
                    this.attributes["accessCount"] = 1;
                }
                
                let message = coinRateMessage(coin, frate, prevFrate, wallet);
                console.info(`${logPrefix(_this, 'GetCoin')} success. coin.name=${coin.name}, rate=${frate}, prev_rate=${prevFrate}, diff=${diffSign} ${diff}%`);
                
                _this.emit(':askWithCard',
                    message + DEFAULT_PROMPT,
                    DEFAULT_PROMPT,
                    cardTitle, cardContent);
            });
        }, (error) => {
            console.error(`${logPrefix(_this, 'GetCoin')} ${error}`);
            _this.emit(':ask', `${coin.name}のレート取得に失敗しました。もう一度レートを知りたいコインの名前をおっしゃって下さい。`);
        });
    },

    'UpdateWallet': function () {
        var _this = this;
        const coinSlot = this.event.request.intent.slots.Coin;
        let coin = lookupCoin(coinSlot);
        if (!coin) {
            _this.emit(':ask', 'コイン名の認識に失敗しました。もう一度所持コイン数の変更をおっしゃって下さい。');

            if (coinSlot && coinSlot.value) {
                console.warn(`${logPrefix(_this, 'UpdateWallet')} Unknown coin [${coinSlot.value}]`);
            } else {
                console.warn(`${logPrefix(_this, 'UpdateWallet')} Unknown coin [No Coin slot]`);
            }
            return;
        }

        var wallet = this.attributes["wallet"];
        if (!wallet) {
            wallet = {};
        }

        var message = "";

        if (wallet[coin.code] || wallet[coin.code] > 0) {
            // 所持コイン有り
            var yourcoin = wallet[coin.code];
            message = `現在${coin.name}を${yourcoin}コイン所持しています。`;
        } else {
            // 所持コイン無し
            message = `現在${coin.name}は所持していません。`;
        }
        message += `コイン所持数の整数部分を読み上げてください。`;

        // コイン更新(整数)にステート遷移
        console.info(`${logPrefix(_this, 'UpdateWallet')} success. coin=${coin.name}, state -> UPDATEWALLET_INT.`);
        this.handler.state = states.UPDATEWALLET_INT;
        this.attributes["update_wallet"] = {
            coin: coin
        };

        this.emit(':ask', message, 'コイン所持数の整数部を読み上げてください。');
    },

    'SessionEndedRequest': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'SessionEndedRequest')} Session exit.`);
        this.attributes['STATE'] = undefined;
        this.attributes['detail'] = undefined;
        this.emit(':saveState', true);
    },

    'Unhandled': function () {
        // ステートのリセット
        var reprompt = 'すいません、よくわかりません。' + DEFAULT_PROMPT;
        this.emit(':ask', reprompt, reprompt);
    }
};

var getDetailHandlers = Alexa.CreateStateHandler(states.GETDETAIL, {
    'AMAZON.YesIntent': function () {
        var _this = this;
        var detail = this.attributes["detail"];

        // ハンドラの実行後、スキルの初期状態に戻すためステートをリセット
        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['detail'] = undefined;

        var messages = detail.map((d) => {
            //return `${d.name} のレートは ${d.rate}円、評価額は${d.jpy}円です。`;
            return coinRateMessage(d.coin, d.rate, d.prevRate, this.attributes["wallet"]);
        });
        var message = messages.join('');
        console.info(`${logPrefix(_this, 'GETDETAIL:YesIntent')} success. state -> default`);

        this.emit(':tell', message + '以上です。またのご利用お待ちしております。');
    },

    // 終了
    'AMAZON.StopIntent': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'GETDETAIL:StopIntent')} success. state -> default`);

        // ステートのリセット
        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['detail'] = undefined;

        this.emit(':tell', GOODBYE_MESSAGE);
    },

    // キャンセル・不明なインテントもNo扱い
    'AMAZON.NoIntent': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'GETDETAIL:NoIntent')} success. state -> default`);

        // ステートのリセット
        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['detail'] = undefined;
        this.emit(':tell', '各通貨レートの確認を中止し、終了します。またのご利用お待ちしております。');
    },
    'AMAZON.CancelIntent': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'GETDETAIL:CancelIntent')} emit NoIntent`);

        this.emitWithState('AMAZON.NoIntent');
    },
    'Unhandled': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'GETDETAIL:Unhandled')} emit NoIntent`);

        this.emitWithState('AMAZON.NoIntent');
    },

    'SessionEndedRequest': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'GETDETAIL:SessionEndedRequest')} Session exit.`);

        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['detail'] = undefined;
        this.emit(':saveState', true);
    }
});

var updateWalletIntHandlers = Alexa.CreateStateHandler(states.UPDATEWALLET_INT, {
    'SpeechNumber': function () {
        var _this = this;

        const numValue = this.event.request.intent.slots.Num.value;
        if (Number.isNaN(numValue) || numValue == '?') {
            console.warn(`${logPrefix(_this, 'UPDATEWALLET_INT:SpeechNumber')} invalid number [${numValue}]`);
            var reprompt = '数値が認識できませんでした。もう一度コイン所持数の整数部を読み上げてください。';
            this.emit(':ask', reprompt, reprompt);
            return;
        }

        var update_wallet = this.attributes['update_wallet'];
        update_wallet["intValue"] = numValue;

        var message = `${numValue}ですね。続けて小数部を読み上げてください`;

        // コイン更新(小数)にステート遷移
        console.info(`${logPrefix(_this, 'UPDATEWALLET_INT:SpeechNumber')} success. intValue=${numValue}, state -> UPDATEWALLET_DEC.`);
        this.handler.state = states.UPDATEWALLET_DEC;
        this.attributes["update_wallet"] = update_wallet;

        this.emit(':ask', message, 'コイン所持数の小数部を読み上げてください。');
    },

    // 終了
    'AMAZON.StopIntent': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'UPDATEWALLET_INT:StopIntent')} state -> default.`);

        // ステートのリセット
        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['update_wallet'] = undefined;

        this.emit(':tell', GOODBYE_MESSAGE);
    },

    // キャンセル
    'AMAZON.CancelIntent': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'UPDATEWALLET_INT:CancelIntent')} state -> default.`);

        // ステートのリセット
        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['update_wallet'] = undefined;

        this.emit(':ask', '所持コインの更新を中止します。' + DEFAULT_PROMPT);
    },

    'SessionEndedRequest': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'UPDATEWALLET_INT:SessionEndedRequest')} Session exit.`);

        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['update_wallet'] = undefined;
        this.emit(':saveState', true);
    },

    'Unhandled': function () {
        var reprompt = 'コイン所持数の整数部を読み上げてください。';
        this.emit(':ask', reprompt, reprompt);
    }
});

var updateWalletDecHandlers = Alexa.CreateStateHandler(states.UPDATEWALLET_DEC, {
    'SpeechNumber': function () {
        var _this = this;

        const numValue = this.event.request.intent.slots.Num.value;
        if (Number.isNaN(numValue) || numValue == '?') {
            console.warn(`${logPrefix(_this, 'UPDATEWALLET_DEC:SpeechNumber')} invalid number [${numValue}]`);
            var reprompt = '数値が認識できませんでした。もう一度コイン所持数の小数部を読み上げてください。';
            this.emit(':ask', reprompt, reprompt);
            return;
        }

        var update_wallet = this.attributes['update_wallet'];
        update_wallet["decValue"] = numValue;

        update_wallet["newCoin"] = Number(`${update_wallet["intValue"]}.${update_wallet["decValue"]}`);
        console.info(`${logPrefix(_this, 'UPDATEWALLET_DEC:SpeechNumber')} success. newCoin=${update_wallet["newCoin"]}, state -> UPDATEWALLET_CONFIRM.`);

        // コイン更新(確認)にステート遷移
        this.handler.state = states.UPDATEWALLET_CONFIRM;

        this.emit(':ask',
            `${update_wallet['coin']['name']}の所持数を${update_wallet['newCoin']}に更新します。よろしいですか？`,
            'はい、もしくはいいえで答えてください。'
        );
    },

    // 終了
    'AMAZON.StopIntent': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'UPDATEWALLET_DEC:StopIntent')} state -> default.`);

        // ステートのリセット
        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['update_wallet'] = undefined;

        this.emit(':tell', GOODBYE_MESSAGE);
    },

    // キャンセル
    'AMAZON.CancelIntent': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'UPDATEWALLET_DEC:CancelIntent')} state -> default.`);

        // ステートのリセット
        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['update_wallet'] = undefined;

        this.emit(':ask', '所持コインの更新を中止します。' + DEFAULT_PROMPT);
    },

    'SessionEndedRequest': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'UPDATEWALLET_DEC:SessionEndedRequest')} Session exit.`);

        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['update_wallet'] = undefined;
        this.emit(':saveState', true);
    },

    'Unhandled': function () {
        var reprompt = 'コイン所持数の小数部を読み上げてください。';
        this.emit(':ask', reprompt, reprompt);
    }
});

var updateWalletConfirmHandlers = Alexa.CreateStateHandler(states.UPDATEWALLET_CONFIRM, {
    'AMAZON.YesIntent': function () {
        var _this = this;
        var update_wallet = this.attributes['update_wallet'];

        // ステートのリセット
        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['update_wallet'] = undefined;

        // ウォレット更新
        var wallet = this.attributes["wallet"];
        if (!wallet) {
            wallet = {};
        }
        wallet[update_wallet['coin']['code']] = update_wallet['newCoin'];
        this.attributes['wallet'] = wallet;

        console.info(`${logPrefix(_this, 'UPDATEWALLET_CONFIRM:YesIntent')} success. wallet[${update_wallet['coin']['code']}]=${update_wallet['newCoin']}. state -> default.`);
        this.emit(':ask', `${update_wallet['coin']['name']}の所持数を${update_wallet['newCoin']}に更新しました。` + DEFAULT_PROMPT);
    },

    // 終了
    'AMAZON.StopIntent': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'UPDATEWALLET_CONFIRM:StopIntent')} state -> default.`);

        // ステートのリセット
        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['update_wallet'] = undefined;

        this.emit(':tell', GOODBYE_MESSAGE);
    },

    'AMAZON.NoIntent': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'UPDATEWALLET_CONFIRM:NoIntent')} state -> default.`);

        // ステートのリセット
        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['update_wallet'] = undefined;
        this.emit(':ask', '所持コインの更新を中止します。' + DEFAULT_PROMPT);
    },
    'AMAZON.CancelIntent': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'UPDATEWALLET_CONFIRM:CancelIntent')} emit NoIntent.`);

        this.emitWithState('AMAZON.NoIntent');
    },

    'SessionEndedRequest': function () {
        var _this = this;
        console.info(`${logPrefix(_this, 'UPDATEWALLET_CONFIRM:SessionEndedRequest')} Session exit.`);

        this.handler.state = '';
        this.attributes['STATE'] = undefined;
        this.attributes['update_wallet'] = undefined;
        this.emitWithState(':saveState', true);
    },
    'Unhandled': function () {
        var reprompt = 'はい、もしくはいいえで答えてください。';
        this.emit(':ask', reprompt, reprompt);
    }
});


exports.handler = function (event, context, callback) {
    const alexa = Alexa.handler(event, context, callback);
    alexa.dynamoDBTableName = process.env.ALEXA_DYNAMODB_TABLE;
    alexa.appId = process.env.ALEXA_APPLICATION_ID;
    alexa.registerHandlers(
        handlers,
        getDetailHandlers,
        updateWalletIntHandlers,
        updateWalletDecHandlers,
        updateWalletConfirmHandlers
    );
    alexa.execute();
};
