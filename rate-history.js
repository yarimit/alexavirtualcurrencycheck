var AWS = require('aws-sdk');

function digit02d(d) {
    return ('0' + d).slice(-2);
}

function RateHistory(tableName, hours) {
    this.documentClient = new AWS.DynamoDB.DocumentClient();
    this.tableName = tableName;
    this.rates = null;

    var dt = new Date();
    dt.setHours(dt.getHours() - hours);
    this.key = `${dt.getFullYear()}-${digit02d(dt.getMonth()+1)}-${digit02d(dt.getDate())}T${digit02d(dt.getHours())}`;
}

RateHistory.prototype = {
    _getRate: function() {
        return new Promise((resolve, reject) => {
            if(this.rates) {
                console.log("retes cached");
                resolve(this.rates);
                return;
            }

            this.documentClient.get({
                TableName: this.tableName,
                Key: {
                    date: this.key
                }
            }, (err, data) => {
                if(err) {
                    reject(err);
                } else {
                    if(data.Item.rates) {
                        this.rates = data.Item.rates;
                        resolve(this.rates);
                    } else {
                        resolve({});
                    }
                }
            });
        });
    },
    getRate: function(code) {
        return new Promise((resolve, reject) => {
            this._getRate().then((rates) => {
                if(rates[code]) {
                    resolve(rates[code]);
                } else {
                    resolve(0);
                }
            }, (err) => {
                resolve(0);
            });
        });
    },
    getRateAll: function() {
        return new Promise((resolve, reject) => {
            this._getRate().then((rates) => {
                resolve(rates);
            }, (err) => {
                resolve({});
            });
        });
    }
};

exports.RateHistory = RateHistory;