const conversation = require('alexa-conversation');
const app = require('../index.js');

const opts = {
  name: 'HelloAlexa Test',
  appId: "APPID",
  app: app,
  handler: app.customHandlerName
};

conversation(opts)
  .userSays('LaunchRequest')
  .plainResponse
  .shouldContain("仮想通貨ポートフォリオスキルへようこそ")
  .end();

conversation(opts)
  .userSays('GetBalance')
  .plainResponse
  .shouldContain("現在の総資産は")
  .end();

conversation(opts)
  .userSays('GetCoin', { 'Coin': 'ビットコイン' })
  .plainResponse
  .shouldContain("ビットコイン のレート")
  .end();

conversation(opts)
  .userSays('GetCoin', { 'Coin': 'ネム' })
  .plainResponse
  .shouldContain("ネム のレート")
  .end();

conversation(opts)
  .userSays('GetCoin', { 'Coin': 'モナ' })
  .plainResponse
  .shouldContain("モナコイン のレート")
  .end();
